const path = require("path");
const express = require("express");
const { nanoid } = require("nanoid");
const Post = require("../models/Post");
const auth = require("../middleware/auth");
const multer = require("multer");
const config = require("../config");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  },
});

const upload = multer({ storage });

const router = express.Router();

router.get("/", async (req, res) => {
  try {
    const post = await Post.find().sort({ datetime: -1 }).populate("author", "username");
    res.send(post);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get("/:id", async (req, res) => {
  try {
    const post = await Post.findOne({ _id: req.params.id }).populate("author", "username");
    res.send(post);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post("/", auth, upload.single("image"), async (req, res) => {
  try {
    if (req.body.description !== "" || req.body.image !== "") {
      const postData = { ...req.body, author: req.user._id, datetime: new Date().toISOString() };

      if (req.file) {
        postData.image = "uploads/" + req.file.filename;
      }

      const post = new Post(postData);
      await post.save();
      return res.send(post);
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;
