const express = require("express");
const Comment = require("../models/Comment");
const auth = require("../middleware/auth");
const router = express.Router();

router.get("/:id", async (req, res) => {
  try {
    const comment = await Comment.find({ post: req.params.id }).populate("author");
    res.send(comment);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post("/", auth, async (req, res) => {
  try {
    const commentData = { ...req.body, author: req.user._id };

    const comment = new Comment(commentData);
    await comment.save();
    res.send(comment);
  } catch (e) {
    res.sendStatus(500);
  }
});
module.exports = router;
