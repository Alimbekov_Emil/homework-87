const mongoose = require("mongoose");
const config = require("./config");
const { nanoid } = require("nanoid");
const User = require("./models/User");
const Post = require("./models/Post");
const Comment = require("./models/Comment");

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (let coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user, admin] = await User.create(
    {
      username: "user",
      password: "12345",
      token: nanoid(),
    },
    {
      username: "admin",
      password: "12345",
      token: nanoid(),
    }
  );

  const [post, post1] = await Post.create(
    {
      title: "Post",
      author: user,
      image: "fixtures/image.jpg",
      datetime: new Date().toISOString(),
      description: "DESCRIPTION POST",
    },
    {
      title: "Post1",
      author: admin,
      image: "fixtures/image1.jpg",
      datetime: new Date().toISOString(),
      description: "DESCRIPTION POST 1",
    }
  );

  await Comment.create(
    { author: user, post: post, text: "Comment" },
    { author: admin, post: post1, text: "Comment1" }
  );

  await mongoose.connection.close();
};

run().catch(console.error);
