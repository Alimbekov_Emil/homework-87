const mongoose = require("mongoose");

const PostSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  datetime: String,
  description: String,
  image: String,
});

const Post = mongoose.model("Post", PostSchema);

module.exports = Post;
