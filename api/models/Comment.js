const mongoose = require("mongoose");

const CommentSchema = new mongoose.Schema({
  author: {
    required: true,
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
  post: {
    required: true,
    type: mongoose.Schema.Types.ObjectId,
    ref: "Post",
  },
  text: String,
});

const Comment = mongoose.model("Comment", CommentSchema);

module.exports = Comment;
