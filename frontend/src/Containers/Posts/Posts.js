import { Grid } from "@material-ui/core";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchPosts } from "../../store/actions/postActions";
import PostItem from "./PostItem";

const Posts = () => {
  const dispatch = useDispatch();

  const posts = useSelector((state) => state.posts.posts);

  useEffect(() => {
    dispatch(fetchPosts());
  }, [dispatch]);

  return (
    <>
      <Grid container direction="column">
        {posts.map((post) => (
          <PostItem
            key={post._id}
            id={post._id}
            title={post.title}
            image={post.image}
            author={post.author.username}
            datetime={post.datetime}
          />
        ))}
      </Grid>
    </>
  );
};

export default Posts;
