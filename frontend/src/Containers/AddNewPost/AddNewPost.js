import { Button, Grid, Typography } from "@material-ui/core";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import FileInput from "../../Components/UI/Form/FileInput";
import FormElement from "../../Components/UI/Form/FormElement";
import { historyPush } from "../../store/actions/historyActions";
import { createPost } from "../../store/actions/postActions";

const AddNewPost = () => {
  const dispatch = useDispatch();
  const [post, setPost] = useState({
    title: "",
    description: "",
    image: "",
  });

  const user = useSelector((state) => state.users.user);

  if (user === null) {
    dispatch(historyPush("/login"));
  }

  const inputChangeHandler = (e) => {
    const { name, value } = e.target;

    setPost((prev) => ({ ...prev, [name]: value }));
  };

  const submitFormHandler = (e) => {
    e.preventDefault();

    const formData = new FormData();

    Object.keys(post).forEach((key) => {
      formData.append(key, post[key]);
    });

    dispatch(createPost(formData));
  };

  const fileChangeHandler = (e) => {
    const name = e.target.name;
    const file = e.target.files[0];

    setPost((prevState) => ({
      ...prevState,
      [name]: file,
    }));
  };

  return (
    <>
      <Typography variant="h3" style={{ margin: "30px 0" }}>
        Add New Post
      </Typography>
      <Grid container spacing={4} direction="column" component="form" onSubmit={submitFormHandler}>
        <FormElement
          label="Title"
          type="text"
          onChange={inputChangeHandler}
          name="title"
          value={post.title}
        />
        <FormElement
          label="Description"
          type="text"
          onChange={inputChangeHandler}
          name="description"
          value={post.description}
        />

        <FileInput name="image" label="Image" onChange={fileChangeHandler} />
        <Button variant="contained" color="secondary" type="submit">
          Create Post
        </Button>
      </Grid>
    </>
  );
};

export default AddNewPost;
