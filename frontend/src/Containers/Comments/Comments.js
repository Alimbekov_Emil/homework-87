import { Card, CardContent, CardHeader, Grid, Typography } from "@material-ui/core";
import moment from "moment";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchComments } from "../../store/actions/commentActions";

const Comments = ({ id }) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchComments(id));
  }, [dispatch, id]);

  const comments = useSelector((state) => state.comments.comments);
  return (
    <>
      <Grid item container direction="column">
        <Typography variant="h5">Comments</Typography>
        {comments.length > 0 ? (
          comments.map((comment) => (
            <Card key={comment._id} style={{ margin: "20px" }}>
              <CardHeader title={"Author:" + comment.author.username} />
              <CardContent>
                <p>Comment: {comment.text}</p>
                <p>Comment Date: {moment(comment.datetime).format("MMMM Do YYYY, h:mm:ss a")}</p>
              </CardContent>
            </Card>
          ))
        ) : (
          <Typography variant="h6">There are no comments</Typography>
        )}
      </Grid>
    </>
  );
};

export default Comments;
