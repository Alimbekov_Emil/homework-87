import React from "react";
import { Route, Switch } from "react-router-dom";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import Posts from "./Containers/Posts/Posts";
import AppToolbar from "./Components/UI/AppToolBar/AppToolBar";
import Register from "./Containers/Register/Register";
import Login from "./Containers/Login/Login";
import AddNewPost from "./Containers/AddNewPost/AddNewPost";
import Post from "./Containers/Posts/Post";

const App = () => {
  return (
    <>
      <CssBaseline />
      <header>
        <AppToolbar />
      </header>
      <main>
        <Container maxWidth="xl">
          <Switch>
            <Route path="/" exact component={Posts} />
            <Route path="/register" component={Register} />
            <Route path="/login" component={Login} />
            <Route path="/new-post" component={AddNewPost} />
            <Route path="/post/:id" component={Post}/>
          </Switch>
        </Container>
      </main>
    </>
  );
};

export default App;
