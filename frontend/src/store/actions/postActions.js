import axiosForum from "../../axiosForum";
import { historyPush } from "./historyActions";

export const FETCH_POST_REQUEST = "FETCH_POST_REQUEST";
export const FETCH_POST_SUCCESS = "FETCH_POST_SUCCESS";
export const FETCH_POST_FAILURE = "FETCH_POST_FAILURE";

export const CREATE_POST_REQUEST = "CREATE_POST_REQUEST";
export const CREATE_POST_SUCCESS = "CREATE_POST_SUCCESS";
export const CREATE_POST_FAILURE = "CREATE_POST_FAILURE";

export const FETCH_SOLOPOST_REQUEST = "FETCH_SOLOPOST_REQUEST";
export const FETCH_SOLOPOST_SUCCESS = "FETCH_SOLOPOST_SUCCESS";
export const FETCH_SOLOPOST_FAILURE = "FETCH_SOLOPOST_FAILURE";

export const fetchSoloPostRequest = () => ({ type: FETCH_SOLOPOST_REQUEST });
export const fetchSoloPostSuccess = (post) => ({ type: FETCH_SOLOPOST_SUCCESS, post });
export const fetchSoloPostFailure = (error) => ({ type: FETCH_SOLOPOST_REQUEST, error });

export const fetchPostRequest = () => ({ type: FETCH_POST_REQUEST });
export const fetchPostSuccess = (posts) => ({ type: FETCH_POST_SUCCESS, posts });
export const fetchPostFailure = (error) => ({ type: FETCH_POST_FAILURE, error });

export const createPostRequest = () => ({ type: CREATE_POST_REQUEST });
export const createPostSuccess = () => ({ type: CREATE_POST_SUCCESS });
export const createPostFailure = (error) => ({ type: CREATE_POST_FAILURE, error });

export const fetchPosts = () => {
  return async (dispatch) => {
    try {
      dispatch(fetchPostRequest());
      const response = await axiosForum.get("/posts");
      dispatch(fetchPostSuccess(response.data));
    } catch (e) {
      dispatch(fetchPostFailure(e));
    }
  };
};

export const fetchSoloPosts = (id) => {
  return async (dispatch) => {
    try {
      dispatch(fetchSoloPostRequest());
      const response = await axiosForum.get("/posts/" + id);
      dispatch(fetchSoloPostSuccess(response.data));
    } catch (e) {
      dispatch(fetchSoloPostFailure(e));
    }
  };
};

export const createPost = (postData) => {
  return async (dispatch, getState) => {
    const token = getState().users.user.token;
    if (token === null) {
      dispatch(historyPush("/login"));
    }
    try {
      dispatch(createPostRequest());
      await axiosForum.post("/posts", postData, { headers: { Authorization: token } });
      dispatch(createPostSuccess());
      dispatch(historyPush("/"));
    } catch (e) {
      dispatch(createPostFailure(e));
    }
  };
};
