import axiosForum from "../../axiosForum";

export const FETCH_COMMENTS_REQUEST = "FETCH_COMMENTS_REQUEST";
export const FETCH_COMMENTS_SUCCESS = "FETCH_COMMENTS_SUCCESS";
export const FETCH_COMMENTS_FAILURE = "FETCH_COMMENTS_FAILURE";

export const POST_COMMENT_REQUEST = "POST_COMMENT_REQUEST";
export const POST_COMMENT_SUCCESS = "POST_COMMENT_SUCCESS";
export const POST_COMMENT_FAILURE = "POST_COMMENT_FAILURE";

export const fetchCommentsRequest = () => ({ type: FETCH_COMMENTS_REQUEST });
export const fetchCommentsSuccess = (comments) => ({ type: FETCH_COMMENTS_SUCCESS, comments });
export const fetchCommentsFailure = (error) => ({ type: FETCH_COMMENTS_FAILURE, error });

export const postCommentRequest = () => ({ type: POST_COMMENT_REQUEST });
export const postCommentSuccess = () => ({ type: POST_COMMENT_SUCCESS });
export const postCommentFailure = (error) => ({ type: POST_COMMENT_FAILURE, error });

export const fetchComments = (id) => {
  return async (dispatch) => {
    try {
      dispatch(fetchCommentsRequest());
      const response = await axiosForum.get("/comments/" + id);
      dispatch(fetchCommentsSuccess(response.data));
    } catch (e) {
      dispatch(fetchCommentsFailure(e));
    }
  };
};

export const postComment = (commentData) => {
  return async (dispatch, getState) => {
    const token = getState().users.user.token;
    try {
      dispatch(postCommentRequest());
      await axiosForum.post("/comments", commentData, { headers: { Authorization: token } });
      dispatch(postCommentSuccess());
      dispatch(fetchComments(commentData.post));
    } catch (e) {
      dispatch(postCommentFailure(e));
    }
  };
};
