import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import thunkMiddleware from "redux-thunk";
import postReducer from "./reducers/postReducer";
import usersReducer from "./reducers/userReducer";
import commentReducer from "./reducers/commentReducer";

const saveToLocalStorage = (state) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem("forumState", serializedState);
  } catch (e) {
    console.log("Could not save state");
  }
};

const loadFromLocalStorage = () => {
  try {
    const serializedState = localStorage.getItem("forumState");
    if (serializedState === null) {
      return undefined;
    }

    return JSON.parse(serializedState);
  } catch (e) {
    return undefined;
  }
};

const rootReducer = combineReducers({
  posts: postReducer,
  users: usersReducer,
  comments: commentReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducer, persistedState, composeEnhancers(applyMiddleware(thunkMiddleware)));

store.subscribe(() => {
  saveToLocalStorage({
    users: store.getState().users,
  });
});

export default store;
