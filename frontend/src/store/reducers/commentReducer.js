import {
  FETCH_COMMENTS_FAILURE,
  FETCH_COMMENTS_REQUEST,
  FETCH_COMMENTS_SUCCESS,
  POST_COMMENT_FAILURE,
  POST_COMMENT_REQUEST,
  POST_COMMENT_SUCCESS,
} from "../actions/commentActions";

const initialState = {
  comments: [],
  commentsLoading: false,
  commentsError: null,
};

const commentReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_COMMENTS_REQUEST:
      return { ...state, commentsLoading: true };
    case FETCH_COMMENTS_SUCCESS:
      return { ...state, commentsLoading: false, comments: action.comments };
    case FETCH_COMMENTS_FAILURE:
      return { ...state, commentsLoading: false, commentsError: action.error };
    case POST_COMMENT_REQUEST:
      return { ...state, commentsLoading: true };
    case POST_COMMENT_SUCCESS:
      return { ...state, commentsLoading: false };
    case POST_COMMENT_FAILURE:
      return { ...state, commentsLoading: false, commentsError: action.error };
    default:
      return state;
  }
};

export default commentReducer;
