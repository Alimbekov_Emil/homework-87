import {
  FETCH_POST_FAILURE,
  FETCH_POST_REQUEST,
  FETCH_POST_SUCCESS,
  FETCH_SOLOPOST_FAILURE,
  FETCH_SOLOPOST_REQUEST,
  FETCH_SOLOPOST_SUCCESS,
} from "../actions/postActions";

const initialState = {
  posts: [],
  soloPost: null,
  postsLoading: false,
  postsError: null,
};

const postReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_POST_REQUEST:
      return { ...state, postsLoading: true };
    case FETCH_POST_SUCCESS:
      return { ...state, postsLoading: false, posts: action.posts };
    case FETCH_POST_FAILURE:
      return { ...state, postsLoading: false, postsError: action.error };
    case FETCH_SOLOPOST_REQUEST:
      return { ...state, postsLoading: true };
    case FETCH_SOLOPOST_SUCCESS:
      return { ...state, postsLoading: false, soloPost: action.post };
    case FETCH_SOLOPOST_FAILURE:
      return { ...state, postsLoading: false, postsError: action.error };
    default:
      return state;
  }
};

export default postReducer;
