import React from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { logoutUser } from "../../../store/actions/userActions";

const UserMenu = ({ user }) => {
  const dispatch = useDispatch();

  return (
    <>
      <b>Hello, {user.username} ! </b>
      <Link style={{ color: "black", textDecoration: "none" }} to="/new-post">
        Add New Post {" "}
      </Link>
      or
      <button
        style={{ background: "inherit", outline: "none", border: "none", cursor: "pointer" }}
        onClick={() => dispatch(logoutUser())}
      >
        Logout
      </button>
    </>
  );
};

export default UserMenu;
