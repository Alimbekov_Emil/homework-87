import React from "react";
import { AppBar, Grid, makeStyles, Toolbar, Typography } from "@material-ui/core";
import { Link } from "react-router-dom";
import AnonymousMenu from "../Menu/AnonymousMenu";
import UserMenu from "../Menu/UserMenu";
import { useSelector } from "react-redux";

const useStyles = makeStyles((theme) => ({
  mainLink: {
    color: "black",
    textDecoration: "none",
    "&:hover": {
      color: "inherit",
    },
  },
  staticToolbar: {
    marginBottom: theme.spacing(2),
  },
}));

const AppToolbar = () => {
  const classes = useStyles();
  const user = useSelector((state) => state.users.user);

  return (
    <>
      <AppBar position="fixed" style={{ background: "#ff8880" }}>
        <Toolbar>
          <Grid container justify="space-between">
            <Grid item>
              <Typography variant="h6">
                <Link to="/" className={classes.mainLink}>
                  Forum
                </Link>
              </Typography>
            </Grid>
            <Grid item>{user ? <UserMenu user={user} /> : <AnonymousMenu />}</Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <Toolbar className={classes.staticToolbar} />
    </>
  );
};

export default AppToolbar;
