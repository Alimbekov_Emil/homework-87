import axios from "axios";
import { apiURL } from "./config";

const axiosForum = axios.create({
  baseURL: apiURL,
});

export default axiosForum;
